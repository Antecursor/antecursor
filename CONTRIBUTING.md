Contribute to the Antecursor Project
====================================

Want to hack on the Antecursor Project? Here is a simple contributor's
guide that explains the contribution process.

This page contains information about reporting issues, adding features
as well as some tips and guidelines useful to experienced open source
contributors.

## Topics

 - Reporting Security Issues
 - Reporting Issues
 - Quick Contribution Tips and Guidelines

## Reporting security issues

The Antecursor maintainers take security seriously. If you discover a
security issue, please bring it to their attention right away!

Please DO NOT file a public issue, instead send your report privately
to antecursor@protonmail.com.

Security reports are greatly appreciated and we will publicly thank you
for it.

## Reporting other issues

A great way to contribute to the project is to send a detailed report when you
encounter an issue. We always appreciate a well-written, thorough bug report,
and will thank you for it!

Check that [our issue database](https://gitlab.com/Antecursor/antecursor/issues)
doesn't already include your problem or suggestion before submitting an issue.
If you find a match, you can use the "subscribe" button to get notified on
updates. Do *not* leave random "+1" or "I have this too" comments, as they
only clutter the discussion, and don't help resolving it. However, if you
have ways to reproduce the issue or have additional information that may help
resolving the issue, please leave a comment.

When reporting issues, always include the project version you're currently using.

Also include the steps required to reproduce the problem if possible and
applicable. This information will help us review and fix your issue faster.
When sending lengthy log-files, consider posting them as a privatebin
(https://privatebin.info/).
Don't forget to remove sensitive data from your logfiles before posting (you can
replace those parts with "REDACTED" or multiple "█").

## Quick contribution tips and guidelines

This section gives the experienced contributor some tips and guidelines.

### Pull requests are always welcome

Not sure if that typo is worth a pull request? Found a bug and know how to fix
it? Do it! We will appreciate it. Any significant improvement should be
documented as [a GitLab issue](https://gitlab.com/Antecursor/antecursor/issues) before
anybody starts working on it.

We are always thrilled to receive pull requests. We do our best to process them
quickly. If your pull request is not accepted on the first try, don't get
discouraged!

### Conventions

Fork the repository and make changes on your fork in a feature branch:

 - If it's a bug fix branch, create an issue with `Kind/Hotfix` label to announce
   your intentions and name it `XXXX-something` where `XXXX` is the number of the issue.
 - If it's a feature branch, create an issue with `Kind/*` label (where the `*` is the
   [type of project branch](https://gitlab.com/Areizen/antecursor/labels)) to announce
   your intentions, and name it `XXXX-something` where `XXXX` is the number of the issue.

**<u>Note:</u>** The GitLab "Create merge request and branch" function can be used to
automate this process.

Write clean code. Universally formatted code promotes ease of writing, reading,
and maintenance.

Pull request descriptions should be as clear as possible and include a reference
to all the issues that they address.

### Successful Changes

Before contributing large or high impact changes, make the effort to coordinate
with the maintainers of the project before submitting a pull request. This
prevents you from doing extra work that may or may not be merged.

Large PRs that are just submitted without any prior communication are unlikely
to be successful.

While pull requests are the methodology for submitting changes to code, changes
are much more likely to be accepted if they are accompanied by additional
engineering work. While we don't define this explicitly, most of these goals
are accomplished through communication of the design goals and subsequent
solutions. It often helps to first state the problem before presenting
solutions.

Typically, the best methods of accomplishing this are to submit an issue,
stating the problem. This issue can include a problem statement and a
checklist with requirements. If solutions are proposed, alternatives should be
listed and eliminated. Even if the criteria for elimination of a solution is
frivolous, say so.

Larger changes typically work best with design documents. These are focused on
providing context to the design at the time the feature was conceived and can
inform future documentation contributions.

### Commit Messages

Commit messages must start with a capitalized and short summary (max. 50 chars)
written in the imperative, followed by an optional, more detailed explanatory
text which is separated from the summary by an empty line.

Commit messages should follow best practices, including explaining the context
of the problem and how it was solved, including in caveats or follow up changes
required. They should tell the story of the change and provide readers
understanding of what led to it.

If you don't understand what we are referring to, please read [How to Write a Git
Commit Message](http://chris.beams.io/posts/git-commit/).

In practice, the best approach to maintaining a nice commit message is to
use `git add -p` and `git commit --amend` to formulate a solid
changeset. This allows one to piece together a change, as information becomes
available.

If you squash a series of commits, don't just submit that. Re-write the commit
message, as if the series of commits was a single stroke of brilliance.

That said, there is no requirement to have a single commit for a PR, as long as
each commit tells the story. For example, if there is a feature that requires a
package, it might make sense to have the package in a separate commit then have
a subsequent commit that uses it.

Remember, you're telling part of the story with the commit message. Don't make
your chapter weird!

### Review

Code review comments may be added to your pull request. Discuss, then make the
suggested modifications and push additional commits to your branch. Post
a comment after pushing. New commits show up in the pull request automatically,
but the reviewers are notified only when you comment.

Pull requests must be cleanly rebased on top of master without multiple branches
mixed into the PR.

**Git tip**: If your PR no longer merges cleanly, use `rebase master` in your
feature branch to update your pull request rather than `merge master`.

Before you make a pull request, squash your commits into logical units of work
using `git rebase -i` and `git push -f`. A logical unit of work is a consistent
set of patches that should be reviewed together: for example, upgrading the
version of a vendored dependency and taking advantage of its now available new
feature constitute two separate units of work. Implementing a new function and
calling it in another file constitute a single logical unit of work. The very
high majority of submissions should have a single commit, so if in doubt: squash
down to one.

### Merge approval

Antecursor maintainers use LGTM (Looks Good To Me) in comments on the code review to
indicate acceptance, or use the GitLab `Status/Ready` label.

