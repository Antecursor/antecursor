Debugging the Antecursor Project
================================

Want to debug the Antecursor Project? Here is a simple contributor's
guide that explains the debugging process.

This pages covers the various mechanisms currently available for
debugging the Antecursor Project.

This documentation has been adapted from [https://github.com/WallarooLabs/wallaroo/blob/master/book/python/debugging.md](WallarooLabs debugging documentation).

## Running the project

The Antecursor Project has been developed using Python 3 libraries and is meant to be compatible with Python 3.7 and above.

If your debugging task doesn't need to perform realtime packet capture, you should consider using the provided
[Docker testbench][#docker-testbench] in order to run the project with a known and reproductible integration environment.

For realtime packet capture, we suggest you to use the last stable [Rapsberry Pi image](SETUP.md#raspberry-image)
and [set up your environment for Antecursor](SETUP.md).

**Please note that all issues must be tested on a supported environment to be processed by our team
(either Docker testbench or Raspberry Pi image).**

## Docker Testbench

A Docker testbench has been developed to allow contributors to perform unit and global tests of the Antecursor Project.

Please note that while the Docker testbench can be used for deffered packet analysis, we aren't guaranteeing its efficiency
for live packet collection and credential gathering since it requires a significant improvement to manage physical devices
within containers.

<u>**Usage example:**</u>

```bash
cd antecursor/
docker-compose build  # build builder and runner images for Antecursor modules
docker-compose run sniffer python /data/sniffer/main.py -d /data/sniffer/sniff.db -p /data/sniffer/data/telnet.pcapng  # run sniffer module with telnet testcase
```

<u>**Note:**</u> The [`.env`](antecursor/.env) file contains `${SNIFFER_DATABASE}` and `${SNIFFER_NIC}` definition and must be updated as needed.

## Debugging using `print` and `repr`

The simplest way to do some debugging would be to include a `print` statement in the code to analyze data.

Here's an example in our `store_credentials` function in our `CredentialEngine` class:

```python
def store_credentials(self, src, dst, type, data):
# ...
    suffix = f"{type}:{data}"
    index = f"{src}:{dst}:{suffix}"
    reverse_index = f"{dst}:{src}:{suffix}"

    if (index not in self.credentials and
        reverse_index not in self.credentials):
        self.credentials[index] = True
        print(f"[+] Found {type} credentials : {data}")
        self.database.insert_credentials(src,dst,type,data)

    return True
```

In this example, we'll be printing the found credentials before we insert them into database.

Using `print` is a very useful way to get logged output that you can return to and analyze after
your program has completed running. However, there are some downsides when using `print` to debug:
you'll need to add a `print` statement everywhere you predict you might need it, you can't get a
"state of the world" look at your application, etc.

Using `print` in Python is not without risks. If you try to print a Unicode string, and your locale
does not support Unicode, you may encounter an error like the following:

```python
>>> u = u'\ua000abcd\u07b4'
>>> print(u)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
UnicodeEncodeError: 'ascii' codec can't encode character u'\ua000' in position 0: ordinal not in range(128)
```

With a streaming input application, you can't always be certain about the contents of the data you try to print.
However, you can effectively avoid this sort of error by printing the [byte representation](https://docs.python.org/2/library/repr.html)
of your data instead, using `print repr(data)`.

In that case, the same code that resulted in an error before will provide a useful printout of the contents of the data object:

```python
>>> u = u'\ua000abcd\u07b4'
>>> print repr(u)
u'\ua000abcd\u07b4'
```

## Debugging Using PDB

If you need a more robust tool to do debugging, the Python standard library provides `pdb`.

`pdb` is an interactive debugger which gives you the option to set breakpoints,
inspect the stack frames, and other features expected from an interactive debugger.

A quick example of using `pdb` in your application would be importing the `pdb` module and
then calling its `set_trace()` function.

<u>**Usage example:**</u>

```python
import pdb
# ...
def application_setup(arg):
    pdb.set_trace()
        # ...
```

The above will insert a breakpoint in the current stack frame and allow you to inspect the
`application_setup` function.

`pdb` comes with a nice set of features so if you're interested in using it, we suggest you to
have a look at the official [documentation](https://docs.python.org/2/library/pdb.html).

## Other Debugging Options

Debugging using `print` or `pdb` is great because they're available on all platforms. However,
we realize you might have your own debugging process.
We encourage you to use the tools that best fit your needs and we are available to help get
you set up if needed.
