# Disclaimer of Warranty

This Project is provided "AS IS."

Any express or implied warranties, including but not limited to, the implied warranties of merchantability and fitness
for a particular purpose are disclaimed.

In no event shall the Antecursor Project team be liable for any direct, indirect, incidental, special, exemplary or consequential
damages (including, but not limited to, procurement of substitute goods or services, loss of use, data or profits, or business interruption)
however caused and on any theory of liability, whether in contract, strict liability, or tort (including negligence or otherwise) arising in
any way out of the use of this Project, even if advised of the possibility of such damage.

The User of this Project agrees to hold harmless and indemnify the Antecursor Project team from every claim or liability (whether in tort or in contract),
including attorney's fees, court costs, and expenses, arising in direct consequence of Recipient's use of the item, including, but not limited to, claims
or liabilities made for injury to or death of personnel of User or third parties, damage to or destruction of property of User or third parties, and infringement
or other violations of intellectual property or technical data rights.

# Disclaimer of Endorsement

Nothing in this Project is intended to constitute an endorsement, explicit or implied, by the Antecursor Project team of any particular manufacturer's product or service.

Reference herein to any specific commercial product, process, or service by trade name, trademark, manufacturer, or otherwise, in this Project does not constitute
an endorsement, recommendation, or favoring by the Antecursor Project team and shall not be used for advertising or product endorsement purposes.

# Notes about GDPR

Neither the Antecursor team nor the contributors to the project can be held responsible for any misuse of the product.
By using Antecursor, you take full responsibility and understand how the product works.
You also understand that you can't use Antecursor without obtaining proper and formal authorization from the network owner.

Stay GDPR compliant. You must at the very least warn every network users about their personal data on the testing period.
Due to its nature while testing, Antecursor does not filter the data, so personal data may be collected and visualized.
Stay cautious about personal data and don't allow any unauthorized reading or collecting on the testing results.
