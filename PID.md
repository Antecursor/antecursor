# Project Initiation Document

This project root document contains the main information you need to get a wide understanding of the Antecursor project.

- [Project Goals](#project-goals)
- [Scope](#scope)
- [Project Organization and Stakeholders](#project-organization-and-stakeholders)
- [Business Case](#business-case)
- [Constraints](#constraints)
- [Risks](#risks)
- [Project Controls](#project-controls)
- [Reporting Frameworks](#reporting-frameworks)
- [PID Sign Off](#pid-sign-off)
- [Summary](#summary)

## Project Goals

The main project goal is the implementation of an autonomous and modular system for network scanning and exploitation.

The system we are making is basically a Raspberry Pi with attached modules that can, when connected to an IT network, discover equipments and connections between them, report vulnerabilites on the system, automatically exploit these vulnerabilities, play scenarios, be commanded remotely.
It works agentless, just connect it to a network and it operates according to the configured mode.

Why ? To challenge ourselves on a cybersecurity project as an integral part of our school curriculum, and maybe to answer a real market need.

## Scope

Since Antecursor is a modular system, it is capable of many functionnalities from network discovering to automated exploitation.
Therefore, these lead to different degrees of penetration in an IT system which must be used accordingly.

It is intended for blue team and red team professionals, as part of their work and tied to a contract describing their own scope on the IT system they are performing in.
It does exclude non legal purposes and intentional harm to either a natural or legal person, through an IT system of their own.

We can't be taken as responsible for uses out of this scope.

## Project Organization and Stakeholders

The team is comprised of 4 skilled members with distinct roles :
- Dorian Andreani: Quality Manager
- Quentin Boulé: Project Manager
- Baptiste Moine ([website](https://www.bmoine.fr/)): Network and System Architect
- Romain Kraft ([website](https://www.areizen.fr/)): Software Engineer

We 4 are French cybersecurity professionals and students, learning in sandwich course during our 3 years in the engineering school ENSIBS (Vannes, France).

This project comes within the scope of our second year at school, with the involvement of other stakeholders :
- Yoann Prioux, technical cybersecurity teacher at ENSIBS: he acts as our customer
- Clément Moine ([website](http://cmoine.fr/)), designer: creates our main illustrations
- José Soler, IT project manager at Thales: he helps and supervises us on project management
- Vincent Corlau, cybersecurity lawyer: he gives us advice on the legal aspect
- ENSIBS teachers: they can provide us advice and help on different matters

## Project Costs

Due to the project nature being a school project, the costs are low.
We can exclude human resources costs because we are students and this project is part of our curriculum.

The main costs are hardware, but are still reasonable since the system is raspberry based.

## Constraints

The main constraint we face is our specific planning.
Overall we work on the project one month on (at school), one month off (at our respective companies).
It's harder to get things done when we don't have the time and we don't see each other.

The milestones are placed accordingly, over 2018 and 2019:
- 23<sup>rd</sup> of November - architecture schema and use cases done, ready for serious business
- 25<sup>th</sup> of January - delivery of a first Antecursor implementation (v0)
- 22<sup>nd</sup> of March - delivery of a second Antecursor version (v1)
- 7<sup>th</sup> of June - delivery of the final Antecursor version (v2)


## Risks

### Customer becomes disengaged with the project

Probability: 1 %

Impact: depending on the reason, it can lead to a complete change of product

Mitigation: we must sell it the right way, Antecursor is for defensive purpose, not for pirates

### Scope misdefined or inaccurate estimates

Probability: 5 %

Impact: may lead to lower our expectations and customer disappointment

Mitigation: keep our mind in the scope and stay realistic

### Dependencies are inaccurate

Probability: 15 %

Impact: change dependency, dramatically change schedule

Mitigation: the hardware and software must be chosen accordingly (state of the art) and realistically

### Stakeholders have inaccurate expectations

Probability: 20 %

Impact: misunderstood project and customer disapointment

Mitigation: communicate and facilitate the understanding

### Project team misunderstands requirements

Probability: 10 %

Impact: gap in project development

Mitigation: communicate between team members

### Resource (time) shortfall

Probability: 40 %

Impact: hard to respect milestone requirements

Mitigation: stay organized and work while being on a company period if needed

### Resources (members) are inexperienced

Probability: 15 %

Impact: mistakes are made

Mitigation: split tasks to the adequate members, with different skills

### Team members with negative attitude towards the project

Probability: 2 %

Impact: project sabotage

Mitigation: stay positive!

### Low team motivation

Probability: 15 %

Impact: low project advancement

Mitigation: shorten goals schedule and tasks to everyone

### Architecture lacks flexibility

Probability: 45 %

Impact: hard to add features and fix bugs from last prototype

Mitigation: have in mind the whole end product when making the architecture

### Architecture is infeasible

Probability: 5 %

Impact: think the architecture again

Mitigation: keep technology compliance and state of the art in mind while making the architecture

### Technology components aren't fit for purpose

Probability: 40 %

Impact: low quality, non scalable component, not interoperable components, too much functionalities offered

Mitigation: choose components according to architecture and specs, no over-engineered component too

### Technology components have security vulnerabilities

Probability: 80 %

Impact: impact on product resistance

Mitigation: check versions and updates for best stability and security

### Components aren't maintanable

Probability: 20 %

Impact: difficulty to master the whole product

Mitigation: choose components with active support and community

### Milestone not respected

Probability: 55 %

Impact: nothing to show during presentation, drop of grade

Mitigation: make prototypes so that at least a part of the work can be shown

### Legal and regulatory change impacts the project

Probability: 3 %

Impact: change in the product direction

Mitigation: ensure a legal watch around the product

### Force Majeure impacts project

Probability: 0.5 %

Impact: the project may be compromised

Mitigation: hope


## Project Controls

The project advancement can be controlled through the meeting of the miletones.

There is no notion of budget nor quality controls.


## Summary

During the 2018-2019 academic year, us 4 cybersecurity students will produce Antecursor, an autonomous and modular plug-and-play network scanner and exploiter, designed for blue and red teams, or even for a SIEM with logs collection.

To do so, 3 main milestones are defined:
- First (v0) Antecursor version: 25<sup>th</sup> of January
- Second (v1) Antecursor version: 22<sup>nd</sup> of March
- Final (v2) Antecursor version: 7<sup>th</sup> of June


