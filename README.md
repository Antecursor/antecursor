Antecursor
==========

<center><img src="docs/static_files/illustrations/antecursor_banner.png" alt="Antecursor Project Banner" title="Antecursor banner" /></center>

The Antecursor Project is an open-source and open-hardware project created by infosec
students with the goal of developing an autonomous and modular system for network
based information gathering and exploitation.

## Audience

Antecursor is intended to help BOTH blue team and red team and can
be used for different purposes:

 - As a versatile tool to automate network reconnaissance/scanning
 - As a validation tool for network devices behavior checking
 - As a training tool for security systems (such as SIEM, IPS and IDS sensors)
   with information gathering and vulnerability exploitation

## Principles

Antecursor is an open project guided by strong principles, aiming to be modular,
flexible and lawful.

 - Modular: Antecursor is intended to include a lots of components that have
   well-defined functions
 - Usable security: Antecursor is intended to provide secure defaults without
   compromising usability
 - Lawful: Antecursor is intended to perform information gathering, but users
   must ensure that they process only the minimum amount of personal data necessary
   to achieve their lawful processing purposes

Antecursor has been developped as a school project, with ethical approach!

## Feature Definitions

Antecursor implements several modes, which can be considered as interaction
level modes:

 1. **Stealthy**: passive information gathering.
    The stealthy mode aims to passively collect data that is shared in the network
    by performing MiTM over twisted pair cables (e.g., FTP credentials, HTTP traffic,
    POP).
 2. **Recon**: active information gathering.
    The recognition mode does the same as the stealth mode but include automated
    network discovery tools (e.g., `nmap`, `netdiscover`, `responder`, `impacket`).
 3. **Exploitation**: automated exploitation and manual operations.
    The exploitation mode aims to exploit known and identified vulnerabilities
    from active or passive mode and allows the Antecursor to be controlled by a
    remote operator (i.e., C&C).

Each mode corresponds to a specific use case of the Antecursor project. By
implementing all these modes, this tool will be able to map the network, exploit
the gathered information, train SIEM/IPS and allow to be
controlled over GSM network by a C&C

The Antecursor Project is intended to be modular and provide an additional
interface to create an offensive script in order to train the SIEM, provide
an additional in-depth training for forensics analysis and assist the red and
blue team professionals.

## License

This project is free software; you can redistribute it and/or modify it under the terms
of the [GNU General Public License](LICENCE) as published by the Free Software Foundation; either
version 3 of the License, or any later version.

This project is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the [project disclaimer](DISCLAIMER.md) or [GNU General Public License](LICENCE) for more details.

## Authors

 - [Romain KRAFT](https://www.areizen.fr/): Software Engineering
 - [Baptiste MOINE](https://www.bmoine.fr/): Network and System Architect
 - Quentin BOULE: Project Manager
 - Dorian ANDREANI: Quality Manager

## Credits

We would like to thank and acknowledge following persons and websites!

 - [Clément MOINE](http://cmoine.fr/): logo and illustrations
