# Antecursor - Sniffer

This tool aims to sniff and capture useful data on a given interface. It will store everything in an [SQLite](https://sqlite.org/index.html) database that will then be processed by the other modules.

## Map the network

This tool will be able to create port mapping and determine a topology of the network from collected data stream.

## Gather credentials

The tool implements a credentials collector, inspired by [net-creds](https://github.com/DanMcInerney/net-creds), that will be able to gather some useful credentials such as FTP, HTTP, SNMP, Kerberos, etc.

## Stay stealthy

It is not necessary to have an assigned IP address to run this tool, you can passively and stealthily collect and analyze data streams that are not encrypted.

## Installation

This tool has been developed using `python3` libraries and is meant to be compatible with Python 3.7 and above:

```bash
python3 -m pip install -r requirements.txt
```

## Usage

```
usage: main.py [-h] -d DATABASE [-i INTERFACE] [-p PCAP] [-t TIME] [-a]
               [-o OUTPUT] [-m {stealthy,recon,exploitation}]

    )
   (_)
  /___\                Antecursor Project
 |  |  |    https://gitlab.com/Antecursor/antecursor
 |__|__|

optional arguments:
  -h, --help            show this help message and exit
  -d DATABASE, --database DATABASE
                        The SQLite file where the database will be stored
  -i INTERFACE, --interface INTERFACE
                        The interface used for sniffing
  -p PCAP, --pcap PCAP  Specify a PCAP as data input
  -t TIME, --time TIME  The time to wait before ending the sniffer
  -a, --append          Append the data to an existing database
  -o OUTPUT, --output OUTPUT
                        Specify a PCAP file to write data
  -m {stealthy,recon,exploitation}, --mode {stealthy,recon,exploitation}
                        Mode to be used by sniffer
```

Example:

```bash
python main.py -i [YOUR_NIC] -d [OUTPUT_DATABASE]
```

## Architecture details

The project files are mainly located in the `src` directory:

```
└── src
    ├── credential_harvester.py # Class that search and store credentials
    ├── database.py             # Class that handle database requests
    ├── network_mapper.py       # Class that will process all useful network interactions
    ├── sniffer.py              # Main class that launch the sniffing process
    └── util
        └── logger.py           # Class used to color log
```

### Output database diagram

![Database diagram](/docs/static_files/illustrations/database_diagram.png)

### Reference

More info can be found in the [global documentation](/docs), section Digging into Antecursor -> Source documentation -> System scripts -> Sniffer
