#!/usr/python3.7
# coding : utf8

import argparse
import sys
import os

from src.database import *
from src.sniffer  import *
from src.scanner.nmap_scan import *
from src.util.socket_handler import *

__author__ = 'Romain KRAFT <romain.kraft@protonmail.com>'

if __name__ == '__main__':

  parser = argparse.ArgumentParser(description='''
    )
   (_)
  /___\                Antecursor Project
 |  |  |    https://gitlab.com/Antecursor/antecursor
 |__|__|

''', formatter_class=argparse.RawTextHelpFormatter)
  parser.add_argument('-d', '--database',
                      type=str,
                      required=True,
                      help='The SQLite file where the database will be stored')
  parser.add_argument('-i', '--interface',
                      type=str,
                      help='The interface used for sniffing')
  parser.add_argument('-p', '--pcap',
                      type=str,
                      help='Specify a PCAP as data input')
  parser.add_argument('-t', '--time',
                      type=int,
                      help='The time to wait before ending the sniffer')
  parser.add_argument('-a', '--append',
                      action='store_true',
                      default=False,
                      help='Append the data to an existing database')
  parser.add_argument('-o', '--output',
                      type=str,
                      help='Specify a PCAP file to write data')
  parser.add_argument('-m', '--mode',
                      default='stealthy',
                      choices=['stealthy', 'recon', 'exploitation'],
                      type=str,
                      help='Mode to be used by sniffer')

  args = parser.parse_args()

  # Checking if root for scapy
  if ( os.geteuid() != 0 ):
    print("ERROR : need to be root")
    exit(-1)  

  # Checking if all required arguments are here \o/
  if( args.database == None ):
    print("ERROR : A database must be specified, exiting ...")
    sys.exit(-1)
  elif ( args.interface == None and args.pcap == None ):
    print("ERROR : An interface or a pcap file must be specified, exiting ...")
    sys.exit(-1)

  # Loading the database
  database = DataBase(args.database, args.append)
  if(not args.append):
    database.create_table_scheme()

  # Starting socket server on the sniffing interface
  if (not args.pcap):
    ip_show = os.popen(f"ip addr show {args.interface}")
    host_ip = ip_show.read().split("inet ")[1].split("/")[0]
    ip_show.close()

    socket = SocketServer(host_ip, 8080)
    socket.start()

  # Starting the sniffer
  sniffer = Sniffer(args.interface, database, args.pcap, args.output, args.mode)
  sniffer.start()

  if (not args.pcap):
    socket.join()

  sniffer.join()
