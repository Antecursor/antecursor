# coding: utf8
from scapy.all import *
from .database import *
from .util.logger import *
from .util.modes import *
from .scanner.nmap_scan import *

__author__ = 'Romain KRAFT <romain.kraft@protonmail.com>'

class NetworkMapper:
  '''
  Class that will map the network according to the packet it receives
  '''

  mode = Mode.PASSIVE
  ip_range = None

  def __init__(self):
    '''
    Initiate the network mapper
    '''
    self.logger = Logger()
    self.dhcp_requests  = []
    self.database = DataBase()
    pass

  def process_dhcp_packet(self,packet):
    """
    This will process the dhcp packet in order to retrieve useful infomrmations
    to map the network successfully.
    
    :param packet: scapy packet containing the dhcp request
    """
    key,message_type = packet[DHCP].options[0]
    if(message_type == 5 ):
      dhcp_request = DHCPInstance()
      for i in packet[DHCP].options:
        if(i == 'end'):
          break
        else:
          key,*value = list(i)
          if(key == "server_id"):
            dhcp_request.server_id = value[0]
          elif(key == "subnet_mask"):
            dhcp_request.subnet_mask = value[0]
          elif(key=="name_server"):
            dhcp_request.name_server = value
          elif(key=="domain"):
            dhcp_request.domain = value[0].decode('ISO-8859-1')
      self.logger.print_colored(self.logger.fg.LIGHTBLUE,repr(dhcp_request))
      self.dhcp_requests.append(dhcp_request)
      self.database.insert_dhcp(dhcp_request)

      # Generating the subnet informations
      ip = ""
      cidr= sum(bin(int(x)).count('1') for x in dhcp_request.subnet_mask.split('.'))
      
      for x in range(4):
        sub_ip = int(dhcp_request.server_id.split('.')[x])
        sub_mask = int(dhcp_request.subnet_mask.split('.')[x])
        ip += str(sub_ip & sub_mask) + "."
      NetworkMapper.ip_range = f"{ip[:-1]}/{cidr}"
      
      if(NetworkMapper.mode == Mode.ACTIVE_SLOW or NetworkMapper.mode == Mode.ACTIVE_QUICK):
        scanner  = NmapScanner()       
        self.launch_scan(scanner)

  def launch_scan(self,scanner):
        if(NetworkMapper.mode == Mode.ACTIVE_QUICK):
          scanner.quick_scan_subnet(NetworkMapper.ip_range)
        elif(NetworkMapper.mode == Mode.ACTIVE_SLOW):
          scanner.slow_scan_subnet(NetworkMapper.ip_range)
