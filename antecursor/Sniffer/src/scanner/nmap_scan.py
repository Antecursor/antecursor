import nmap
import sys

from src.database import *

class NmapScanner:
    '''
    The aim of this class is to scan more agressively the subnet found by monitoring the DHCP requests
    It use `python-nmap` (https://bitbucket.org/xael/python-nmap)

    There two scan mode, one slow that will find each hosts and each ports of the hosts found, according to the subnet
    length this can be very time consuming.

    The second one aim to quickly scan the overall network and have a quick view of each ports found.
    '''

    def __init__(self):
        self.database = DataBase()

    def quick_scan_subnet(self, ip_range):
        '''
        Will scan the subnet very quickly to find hosts and common portsv
         -v: (optional) include down hosts to the output
        -T[0-5]: either "Paranoid", "Sneaky", "Polite", "Normal", "Aggressive" or "Insane" timing mode
        -sn: skip port scan
        -n: skip reverse DNS resolution
        -PR: scan ethernet hosts using ARP
        -oX -: output will be formatted to XML and printed on stdout
        -Pn: skip host discovery
        -sS: SYN scan port discovery (fast but can be blocked by firewall, -sT is slower and paranoid but can be used alternatively with --reason)
        --top-ports=1000: scan the top 1000 most popular ports
        -sV: enable version detection
        --version-intensity=[0-9]: specify the intensity level of version detection (the higher the intensity, the more longer the detection will take)
    
        :param ip_range: subnet_base_ip/cidr
        '''
        nm = nmap.PortScanner()
        print(f"Start Scanning {ip_range}")
       
        # First scan: quick scan to have all IPs up

        nm.scan(hosts=ip_range, arguments="-v -T5 -sn -n -PR")
        
        all_hosts =  nm.all_hosts()

        # - filtering all hosts that are up
        hosts = list(filter( lambda x: nm[x]['status']['state'] == "up", all_hosts))
        print(f"[+] Found hosts : {hosts}")

        print("Scanning common ports for found hosts")
        # Second scan : top 1000 port finding
        nm = nmap.PortScanner()
        nm.scan(hosts=" ".join(hosts), arguments="-v -T5 -sS -sV --version-intensity=5")
        print(nm.command_line())
        
        #Printing the elements found and adding them to the database
        for host in nm.all_hosts():
            print(host)
            for proto in nm[host].all_protocols():
                print('----------')
                print('Protocol : %s' % proto)
                lport = nm[host][proto].keys()
                lport = sorted(lport)
                for port in lport:
                    print ('port : %s\tstate : %s' % (port, nm[host][proto][port]['state']))
                    self.database.insert_port(host,port,proto)
        print("Added result to the database")





    def slow_scan_subnet(self, ip_range):
        '''
        Will scan the subnet very slowly to find all hosts and all ports

        :param ip_range: subnet_base_ip/cidr
        '''
        nm = nmap.PortScanner()
        print(f"Start Scanning {ip_range}")
       
        # First scan: quick scan to have all IPs up
        nm.scan(hosts=ip_range, arguments="-v -T5 -sn")
        
        all_hosts =  nm.all_hosts()

        # - filtering all hosts that are up
        hosts = list(filter( lambda x: nm[x]['status']['state'] == "up", all_hosts))
        print(f"[+] Found hosts : {hosts}")

        print("Scanning common ports for found hosts")
        # Second scan : top 1000 port finding
        nm = nmap.PortScanner()
        nm.scan(hosts=" ".join(hosts), arguments="-v -T4 -sS -p- ")
        print(nm.command_line())

        # Printing the elements found and adding them to the database
        for host in nm.all_hosts():
            print(host)
            for proto in nm[host].all_protocols():
                print('----------')
                print('Protocol : %s' % proto)
                lport = nm[host][proto].keys()
                lport = sorted(lport)
                for port in lport:
                    print ('port : %s\tstate : %s' % (port, nm[host][proto][port]['state']))
                    self.database.insert_port(host,port,proto)
        print("Added result to the database")