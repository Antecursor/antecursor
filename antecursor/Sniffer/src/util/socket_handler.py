import socket
import threading
import time
from src.network_mapper import *
from src.util.modes import *
from src.database import *
from src.scanner import *

import base64


class SocketServer(threading.Thread):
    '''
    The class SocketServer used to process connections for modifying the mode
    '''
    def __init__(self,host,port):
        threading.Thread.__init__(self) 
        self.host = host
        self.port = port

    def run(self):
        '''
        Function called when starting a Thread, this function will set up a TCP server on port 8080
        and process each request to this TCP port. Allowing Antecursor to be remotely controlled
        '''
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.bind((self.host, self.port))

        while True:
                sock.listen(5)
                client, address = sock.accept()
              

                response = client.recv(255).strip().decode("utf8")
                if response != "":
                        print(f"{address} connected and send command {response}")
                        if(response == "ACTIVEQ"):
                            NetworkMapper.mode = Mode.ACTIVE_QUICK
                            print(f"Mode {Mode.ACTIVE_QUICK}")
                        elif(response == "ACTIVES"):
                            NetworkMapper.mode = Mode.ACTIVE_SLOW
                            print(f"Mode {Mode.ACTIVE_SLOW}")
                        elif(response == "PASSIVE"):
                            NetworkMapper.mode = Mode.PASSIVE
                            print(f"Mode {Mode.PASSIVE}")
                        elif(response == "GET_DATABASE"):
                            database = DataBase()
                            f = open(database.database_path,"rb")
                            client.send(base64.b64encode(f.read()))
                        elif(response == "START_SCAN"):
                            print(NetworkMapper.ip_range)
                            if(NetworkMapper.mode == Mode.ACTIVE_SLOW or NetworkMapper.mode == Mode.ACTIVE_QUICK and NetworkMapper.ip_range):
                                client.send(b"started scanning")
                                scanner = NmapScanner()
                                network_mapper = NetworkMapper()
                                network_mapper.launch_scan(scanner)
                                client.send(b"Scan finished, you can dump the database")
                            else:
                                client.send(b"No dhcp found, unable to start scan for the moment")
                        client.close()
                        
        print("Close")
        client.close()
        sock.close()