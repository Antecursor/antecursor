#!/bin/sh

set -u
set -e

# Add a console on tty1
if [ -e ${TARGET_DIR}/etc/inittab ]; then
    grep -qE '^tty1::' ${TARGET_DIR}/etc/inittab || \
	sed -i '/GENERIC_SERIAL/a\
tty1::respawn:/sbin/getty -L  tty1 0 vt100 # HDMI console' ${TARGET_DIR}/etc/inittab
fi


cat <<-EOF >${TARGET_DIR}/etc/build-id
Commit: $(git rev-parse HEAD)
Branch: $(git branch | grep \* | awk '{print $2}')
Build time: $(date)

EOF

SSH_PATH=.ssh
SSH_KEY=${SSH_PATH}/antecursor_rsa
mkdir -p ${SSH_PATH}/
[ -f ${SSH_KEY} ] && rm -f ${SSH_KEY} ${SSH_KEY}.pub
ssh-keygen -N '' -t rsa -b 4096 -o -a 100 -f ${SSH_KEY}
mkdir -p ${TARGET_DIR}/root/.ssh/
cat ${SSH_KEY}.pub >${TARGET_DIR}/root/.ssh/authorized_keys
if grep buildroot ${SSH_PATH}/config >/dev/null; then
  perl -p -i -e 'BEGIN{undef $/;} s@(Host buildroot[^/]+.*IdentityFile ).*@${1}'${SSH_KEY}'@' ~/.ssh/config
else
  cat <<-EOF >>${SSH_PATH}/config
Host buildroot
  Hostname 192.168.0.1
  Port 22
  User root
  IdentityFile ${SSH_KEY}

EOF
fi

