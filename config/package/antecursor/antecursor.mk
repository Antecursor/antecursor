################################################################################
#
# antecursor
#
################################################################################

ANTECURSOR_VERSION = dev
ANTECURSOR_SITE = https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/Antecursor/antecursor.git

ANTECURSOR_DEPENDENCIES = python3 python-scapy3k sqlite
ANTECURSOR_SITE_METHOD = git

define ANTECURSOR_INSTALL_TARGET_CMDS
    mkdir -p $(TARGET_DIR)/opt/antecursor
    rsync -av $(@D)/antecursor/ $(TARGET_DIR)/opt/antecursor/
    $(INSTALL) -D -m 755 package/antecursor/antecursor.cfg  $(BINARIES_DIR)/antecursor.cfg
endef

$(eval $(generic-package))
