################################################################################
#
# python-nmap
#
################################################################################

PYTHON_NMAP_VERSION = 0.6.1
PYTHON_NMAP_SOURCE = python-nmap-$(PYTHON_NMAP_VERSION).tar.gz
PYTHON_NMAP_SITE = https://files.pythonhosted.org/packages/dc/f2/9e1a2953d4d824e183ac033e3d223055e40e695fa6db2cb3e94a864eaa84
PYTHON_NMAP_SETUP_TYPE = distutils
PYTHON_NMAP_LICENSE = GNU General Public License (GPL)

$(eval $(python-package))
