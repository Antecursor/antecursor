# Antecursor Documentation

Currently, the Antecursor doesn't provide hosted documentation.
Officially, you may only build it and read it locally (or read the source directly, although it's not the most common way).

The documentation is made with [Sphinx](http://www.sphinx-doc.org/) and displayed with [Read the Docs theme](https://github.com/rtfd/sphinx_rtd_theme).

# Build from host

## Requirements

You will need Python (tested with Python 3) to build the documentation.

```shell
python -m pip install -r requirements.txt
```

## Usage

Linux users:

```shell
make html
```

Windows users:
```shell
make.bat html
```

Then the docs are available from `build/html/index.html`.
