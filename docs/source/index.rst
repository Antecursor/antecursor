.. This index page is the very first page the user must see.
   It just helps him to navigate in the documentation and implements the toctree.

Welcome to Antecursor's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Table of contents

   intro
   userman
   tech
