.. This page intends to introduce the product, the team and the project context
   to the unaware user.
   It is intended for the person who doesn't know the product yet.

.. _antecursor-overview:

===========================
Antecursor Project Overview
===========================

About Antecursor
================

.. image:: ../static_files/illustrations/antecursor_banner.png
   :scale: 70%
   :align: center

The Antecursor Project is an open-source and open-hardware project created by infosec
students with the goal of developing an autonomous and modular system for network
based information gathering and exploitation.

Audience
--------

Antecursor is intended to help BOTH blue team and red team and can be used for different purposes:

* As a versatile tool to automate network reconnaissance/scanning
* As a validation tool for network devices behavior checking
* As a training tool for security systems (such as SIEM, IPS and IDS sensors) with information gathering and vulnerability exploitation

Principles
----------

Antecursor is an open project guided by strong principles, aiming to be modular,
flexible and lawful.

* Modular: Antecursor is intended to include a lots of components that have well-defined functions
* Usable security: Antecursor is intended to provide secure defaults without compromising usability
* Lawful: Antecursor is intended to perform information gathering, but users must ensure that they process only the minimum amount of personal data necessary to achieve their lawful processing purposes

Antecursor has been developped as a school project, with ethical approach!

Feature Definitions
-------------------

Antecursor implements several modes, which can be considered as interaction
level modes:


1. **Stealthy**: passive information gathering. The stealthy mode aims to passively collect data that is shared in the network by performing MiTM over twisted pair cables (e.g., FTP credentials, HTTP traffic, POP).
2. **Recon**: active information gathering. The recognition mode does the same as the stealth mode but include automated network discovery tools (e.g., ``nmap``, ``netdiscover``, ``responder``, ``impacket``).
3. **Exploitation**: automated exploitation and manual operations. The exploitation mode aims to exploit known and identified vulnerabilities from active or passive mode and allows the Antecursor to be controlled by a remote operator (i.e., C&C).

Each mode corresponds to a specific use case of the Antecursor project. By implementing all these modes, this tool will be able to map the network, exploit
the gathered information, train SIEM/IPS and allow to be controlled over GSM network by a C&C.

The Antecursor Project is intended to be modular and provide an additional interface to create an offensive script in order to train the SIEM, provide
an additional in-depth training for forensics analysis and assist the red and blue team professionals.

License
-------

This project is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This project is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, see http://www.gnu.org/licenses.

About the Antecursor team
=========================

We are 4 students in the Cybersecurity branch of the ENSIBS school (Vannes, France).
Not only we study infosec, but we also work in parallel for a specific company, alternating between course phase and work phase (~1 month long).

Authors
-------

* `Romain KRAFT <https://areizen.fr/>`_ (La Poste Group): Software Engineer
* `Baptiste MOINE <https://bmoine.fr/>`_ (Ministry of Armed Forces): Network and System Architect
* Quentin BOULE (Crédit Agricole Group): Project Manager
* Dorian ANDREANI (ENEDIS): Quality Manager

Credits
-------

We would like to thank and acknowledge following persons and websites!

* `Clément MOINE <https://cmoine.fr/>`_: logo and illustrations
* Yoann PRIOUX: for his role of the customer
* Vincent CORLAU: for his expertise in cybersecurity law
* José SOLER: for his help on project management methodology

About the school project
========================

The Antecursor project is a part of our school course: from September 2018 to June 2019 (with other courses and work phases in parallel), we chose to make Antecursor in our 4-members team.

Milestones
----------

We have to respect the following milestones and choices we have made:

- W39 Introduction: introduce the project concepts to the customer
- W47 Kickoff: frame the project with architecture diagrams and use cases
- W04 V0: make a product PoC, implement the stealthy mode
- W12 V1: improve the product, implement the recon mode and make the PoE module operational
- W23 V2: finalize the product, implement the exploitation mode and make the GSM module operational

.. mermaid::

   gantt
      dateFormat DD/MM/YY
      title Antecursor project: overview

      section Introduction
      Think the project :done, i1, 01/09/18, 24/09/18
      Role distribution :done, i2, 01/09/18, 24/09/18
      Setup tools :done, i3, 01/09/18, 24/09/18

      section Kickoff
      Antecursor use cases :done, k1, 24/09/18, 23/11/18
      Physical product architecture  :done, k2, 24/09/18, 23/11/18
      Logical product architecture   :done, k3, 24/09/18, 23/11/18

      section V0
      Antecursor V0 :done, v01, 23/11/18, 24/01/19
      Poster V0 :done, v02, 23/11/18, 24/01/19
      Documentation V0 :done, v03, 23/11/18, 24/01/19

      section V1
      Antecursor V1 :active, v11, 24/01/19, 22/03/19
      Poster V1 :active, v12, 24/01/19, 22/03/19
      Documentation V1 :active, v13, 24/01/19, 22/03/19
      User manual V0 :active, v14, 24/01/19, 22/03/19

      section V2
      Antecursor V2 :v21, 22/03/19, 07/05/19
      Poster V2 (EN / FR) :v22, 22/03/19, 07/05/19
      Documentation V2 :v23, 22/03/19, 07/05/19
      User manual V1 :v24, 22/03/19, 07/05/19
      Qualification tests folder :v25, 22/03/19, 07/05/19

.. _disclaimer:

Disclaimers
===========

Disclaimer of Warranty
----------------------

This Project is provided "AS IS."

Any express or implied warranties, including but not limited to, the implied warranties of merchantability and fitness for a particular purpose are disclaimed.

In no event shall the Antecursor Project team be liable for any direct, indirect, incidental, special, exemplary or consequential
damages (including, but not limited to, procurement of substitute goods or services, loss of use, data or profits, or business interruption)
however caused and on any theory of liability, whether in contract, strict liability, or tort (including negligence or otherwise) arising in
any way out of the use of this Project, even if advised of the possibility of such damage.

The User of this Project agrees to hold harmless and indemnify the Antecursor Project team from every claim or liability (whether in tort or in contract),
including attorney's fees, court costs, and expenses, arising in direct consequence of Recipient's use of the item, including, but not limited to, claims
or liabilities made for injury to or death of personnel of User or third parties, damage to or destruction of property of User or third parties, and infringement
or other violations of intellectual property or technical data rights.

Disclaimer of Endorsement
-------------------------

Nothing in this Project is intended to constitute an endorsement, explicit or implied, by the Antecursor Project team of any particular manufacturer's product or service.

Reference herein to any specific commercial product, process, or service by trade name, trademark, manufacturer, or otherwise, in this Project does not constitute
an endorsement, recommendation, or favoring by the Antecursor Project team and shall not be used for advertising or product endorsement purposes.

Notes about GDPR
----------------

Neither the Antecursor team nor the contributors to the project can be held responsible for any misuse of the product.
By using Antecursor, you take full responsibility and understand how the product works.
You also understand that you can't use Antecursor without obtaining proper and formal authorization from the network owner.

Stay GDPR compliant. You must at the very least warn every network users about their personal data on the testing period.
Due to its nature while testing, Antecursor does not filter the data, so personal data may be collected and visualized.
Stay cautious about personal data and don't allow any unauthorized reading or collecting on the testing results.

FAQ
===

Where can I find the project?
-----------------------------

The full source project can be found in the `Gitlab project <https://gitlab.com/Antecursor>`_.

Why *Antecursor*?
-----------------

Antecursor means **scout** in Latin. It fits well the slogan |slogan| and the logo |logo|, don't you think ?

Can I use Antecursor to hack someone?
-------------------------------------

No. Read the :ref:`disclaimer`. You're the only responsible for your own actions and for any misuse of Antecursor.
