.. This page intends to make the user/developer understand
   how the different components of the product work "behind
   the scene". It also contains the development documentation.
   It is intended for the user who wants to go into the development in depth.

=======================
Digging into Antecursor
=======================

.. note::

   It's recommended to understand the :ref:`Antecursor Project <antecursor-overview>` before reading the following.

This page provides information about how Antecursor works behind the scenes.

Overview and architectures
==========================

Functional architecture
-----------------------

Being autonomous, the Antecursor system discovers by itself the black box environment where it is placed.
Like a Man-in-the-Middle, the Antecursor system listens to the data flow passing through, pretending to be the target computer it is plugged to.

.. image:: ../static_files/illustrations/functional_architecture.png

Logical architecture
--------------------

In an Antecursor running environment, you can distinguish 4 actors:

- The **Antecursor system** itself, which interacts with the 3 others as described below.
- The **target computer** (or **secretary computer**) that covers the Antecursor system identity and which is part of the corporate network
- The **corporate network** that the Antecursor system process according to its configured mode
- The **GSM or Wi-Fi Network** that can give orders to the Antecursor system and exfiltrate the data collected, outside the corporate network

.. image:: ../static_files/illustrations/logical_architecture.png

Physical architecture
---------------------

The Antecursor system is composed of a Raspberry Pi 3 as its core component, with a micro SD card as storage.

To connect the network and the secretary computer, 2 RJ45 cables are required: network on the integrated Raspberry Pi 3 Ethernet port (potentially PoE) and secretary with a USB adapter.
For exfiltration to be available, a GSM module is required, or the integrated WiFi controller can be used.

To charge the Antecursor system, it is Power over Ethernet compatible (on the integrated Raspberry Pi 3 Ethernet port), or use a power bank, or the local power supply.

.. image:: ../static_files/illustrations/physical_architecture.png

System configuration
====================

System initialization
---------------------

When the Antecursor system is first plugged between the network and the target computer (or secretary computer), it follows the next steps.

First is spoofing the MAC address of the target computer to fool the network:

.. image:: ../static_files/illustrations/spoofing.png

Network configuration
"""""""""""""""""""""

.. mermaid::

   sequenceDiagram
      participant Target Computer
      participant Antecursor
      participant Corporate Network
      Antecursor->>Target Computer: Ethernet port connection
      Antecursor->>Corporate Network: Ethernet port connection
      Antecursor->>Target Computer: Ethernet port activation
      Note over Antecursor, Target Computer: ARP exchange<br/>(target computer's MAC address<br/>is recorded)
      Target Computer->>+Antecursor: DHCP Discover
      Antecursor-->>-Target Computer: DHCP Offer
      Target Computer->>+Antecursor: DHCP Request
      Antecursor-->>-Target Computer: DHCP ACK
      Note over Antecursor, Target Computer: The Antecursor is visible to the<br/>target computer as a generic<br/>network router (with DHCP, DNS,<br/>routing services)
      Note over Antecursor: Post-lease hook<br/>script (each time we<br/>get client)
      Antecursor->>Antecursor: MAC address changing (target spoofing)
      Antecursor->>Corporate Network: Ethernet port activation
      Antecursor->>+Corporate Network: DHCP Discover
      Corporate Network-->>-Antecursor: DHCP Offer
      Antecursor->>+Corporate Network: DHCP Request
      Corporate Network-->>-Antecursor: DHCP ACK
      Note over Antecursor, Corporate Network: The Antecursor is visible to the<br/>corporate network as the original<br/>target computer
      Antecursor->>Antecursor: DHCPd configuration update (DNS, domain, etc.)
      Target Computer->>+Antecursor: DNS Request
      Antecursor->>+Corporate Network: DNS Request (forwarding)
      Corporate Network->>-Antecursor: DNS Response
      Antecursor->>-Target Computer: DNS Response
      Target Computer->>+Antecursor: Any TCP/UDP packet
      Note over Antecursor, Corporate Network: Basic routing using NAT

At the end of the process, the routing tables are like:

* for the **target computer**:

============ =========== =========== ====================================
 IP address  MAC address  Interface           Description                
============ =========== =========== ====================================
  dynamic    ``@MAC A``   ``eth0``    Connected to the Antecursor system
============ =========== =========== ====================================

* for the **Antecursor system**:

=============== =========================================== =========== ====================================
   IP address                  MAC address                   Interface              Description             
=============== =========================================== =========== ====================================
  192.168.0.1    ``@MAC B``                                  ``eth0``    Connected to the Target Computer   
--------------- ------------------------------------------- ----------- ------------------------------------
    dynamic      ``@MAC A`` (spoofed from target computer)   ``eth1``    Connected to the Corporate Network 
=============== =========================================== =========== ====================================

.. _system-scripts-doc:

System scripts
==============

Sniffer
-------

Description
"""""""""""

This tool aims to **sniff** and **capture** useful data on a given interface.
It stores everything in the :ref:`SQLite Database <database-doc>` that is intended to be processed by the other modules.

It is able to map the network and determine the network topology from the data collected.
The Sniffer may be run in stealthy-mode-doc since it just passively grabs the data passing through.

From the requests flow, the script stores every:

- IP Addresses from any protocol
- Ports from any protocol
- Credentials from specific protocols such as FTP, HTTP, SNMP
- Other output specified in the :ref:`database documentation <database-doc>` below

.. note:: The credential gathering is inspired by `net-creds <https://github.com/DanMcInerney/net-creds>`_.

In recon mode, the Sniffer will automatically start a nmap scan when a DHCP request is discovered..

It can also receive commands, see *Change config on the fly* in the user guide part.

Usage
"""""

.. code-block:: none

 usage: main.py [-h] -d DATABASE [-i INTERFACE] [-p PCAP] [-t TIME] [-a]
                [-o OUTPUT] [-m {stealthy,recon,exploitation}]
 
     )
    (_)
   /___\                Antecursor Project
  |  |  |    https://gitlab.com/Antecursor/antecursor
  |__|__|
 
 optional arguments:
   -h, --help            show this help message and exit
   -d DATABASE, --database DATABASE
                         The SQLite file where the database will be stored
   -i INTERFACE, --interface INTERFACE
                         The interface used for sniffing
   -p PCAP, --pcap PCAP  Specify a PCAP as data input
   -t TIME, --time TIME  The time to wait before ending the sniffer
   -a, --append          Append the data to an existing database
   -o OUTPUT, --output OUTPUT
                         Specify a PCAP file to write data
   -m {stealthy,recon,exploitation}, --mode {stealthy,recon,exploitation}
                         Mode to be used by sniffer

Example:

.. code-block:: none

 python main.py -i [INTERFACE] -d [OUTPUT_DATABASE]

.. _database-doc:

SQLite Database
---------------

The database is generated by the :ref:`system scripts <system-scripts-doc>` when the Antecursor system is in its running phase.

It can be exfiltrated and viewed using the :ref:`web interface <web-interface-doc>`.

Architecture
""""""""""""

.. image:: ../static_files/illustrations/database_diagram.png
   :align: left  

.. _web-interface-doc:

Web interface
=============

The web interface comes with many views to visualize the database.
It's used after the database was exfiltrated as a client-only web interface.

It is written in JS, using JQuery and Bootstrap 4 for the view.

Below is an insight of the web interface model.

.. image:: ../static_files/illustrations/web_interface_model.png

API Reference
=============

Indices
-------

* :ref:`genindex`
* :ref:`modindex`

API modules
-----------

.. automodule:: src.sniffer
   :members:

.. automodule:: src.credential_harvester
   :members:

.. automodule:: src.database
   :members:

.. automodule:: src.network_mapper
   :members:
