#!/bin/bash

# Download the latest antecursor firmware build.

set -o errexit

function download() {
    ACCESS_TOKEN=${1}
    REF=${2}
    DEST_PATH=${3}
    JOB=antecursor-build
    NS=Antecursor/antecursor
    ID=8506729
    BASE_PATH=$(dirname $(readlink -f $0))
    FILE_PATH=${DEST_PATH}/${JOB}-${REF}.zip
    LAST_ID_FILE=${DEST_PATH}/.last_${REF}

    echo "[+] Fetching latest release job id..."
    JOB_ID="$(python ${BASE_PATH}/get_last_release_id.py -r ${REF} -i ${ID} -t ${ACCESS_TOKEN})"

    set +o errexit
    PREV="$(cat ${LAST_ID_FILE} 2>/dev/null)"
    set -o errexit

    if [[ ${PREV} != ${JOB_ID} ]]; then
        echo "[+] New version (job id: ${JOB_ID}) for ${REF} branch!"
        DOWNLOAD=1
    else
        if [ -f ${FILE_PATH} ]; then
            DOWNLOAD=0
            echo "[+] No updates..."
            EXIT_CODE=1
        else
            DOWNLOAD=1
            EXIT_CODE=0
        fi
    fi

    if ((${DOWNLOAD} == 1)); then
        FW_PROVIDER=https://gitlab.com/api/v4/projects/${ID}/jobs/${JOB_ID}/artifacts
        mkdir -p ${DEST_PATH}/
        curl --fail --location --header "PRIVATE-TOKEN: ${ACCESS_TOKEN}" "${FW_PROVIDER}" -o ${FILE_PATH}
    fi

    echo "${JOB_ID}" >${LAST_ID_FILE}

    return ${EXIT_CODE}
}

if ((${#} < 3)); then
    echo "Usage: ${0} access_token branch dest_path"
else
    # Example:
    # ./download_last_build.sh ${ACCESS_TOKEN} dev builds/
    download ${1} ${2} ${3}

    exit ${?}
fi
