#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

import requests, json
import argparse

def parse_args():
    """Arguments parsing."""
    parser = argparse.ArgumentParser()

    parser.add_argument('-i', '--id',
                        type=int,
                        default='8506729',
                        help='project id')

    parser.add_argument('-r', '--ref',
                        type=str,
                        default='master',
                        help='project ref (branch)')

    parser.add_argument('-t', '--token',
                        type=str,
                        required=True,
                        help='project ref (branch)')

    args = parser.parse_args()

    return args

def get_last_pipeline(p_id, p_ref, u_token):
    sess = requests.Session()
    json_ = json.JSONDecoder()

    sess.headers.update({'PRIVATE-TOKEN': u_token})
    data = sess.get('https://gitlab.com/api/v4/projects/{0}/pipelines?scope=finished&status=success&order_by=id&sort=desc&ref={1}'.format(p_id, p_ref))

    latest = json_.decode(data.text)[0]

    return latest

def get_last_job(p_id, p_ref, u_token, pl_id):
    sess = requests.Session()
    json_ = json.JSONDecoder()

    sess.headers.update({'PRIVATE-TOKEN': u_token})
    data = sess.get('https://gitlab.com/api/v4/projects/{0}/pipelines/{1}/jobs?scope=success'.format(p_id, pl_id))

    latest = json_.decode(data.text)[0]

    return latest

def main():
    try:
        # Arguments parsing.
        args = parse_args()

        # Print last release id.
        last_pipeline = get_last_pipeline(args.id, args.ref, args.token)
        last_job = get_last_job(args.id, args.ref, args.token, last_pipeline['id'])
        print(last_job['id'])
    except (ValueError, TypeError, OSError, IOError) as e:
        print(e)

## Runtime processor.
if __name__ == '__main__':
    main()
