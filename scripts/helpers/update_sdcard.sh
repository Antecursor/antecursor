#!/bin/bash

# Upgrade the antecursor firmware using sdcard image.

set -o errexit

function upgrade() {
    IMAGE_FILE=${1}
    RPI_IP=${2}
    DEST_IMG=/tmp/sdcard.img
    DEVICE=/dev/mmcblk0

    echo "[+] Uploading system image on the remote Raspberry Pi..."
    scp -o StrictHostKeyChecking=no ${IMAGE_FILE} root@${RPI_IP}:${DEST_IMG}
    echo "[+] Unmounting remote file system..."
    ssh -o StrictHostKeyChecking=no root@${RPI_IP} 'kill -9 $(ps | grep antecursor | grep -v grep | awk '"'"'{print $1}'"'"'); umount /data; umount /boot'
    echo "[+] Flashing the remote sd-card with system image..."
    ssh -o StrictHostKeyChecking=no root@${RPI_IP} "dd bs=4M if=${DEST_IMG} of=${DEVICE} conv=fsync"
    echo "[+] Rebooting the remote Raspberry Pi..."
    ssh -o StrictHostKeyChecking=no root@${RPI_IP} "/sbin/reboot"
}

if ((${#} < 2)); then
    echo "Usage: ${0} file_img rpi_ip"
else
    # Example:
    # ./update_sdcard.sh ../buildroot/output/images/sdcard.img 192.168.0.1
    upgrade ${1} ${2}
fi
