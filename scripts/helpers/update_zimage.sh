#!/bin/bash

# Upgrade the antecursor firmware using zImage.

set -o errexit

function upgrade() {
    IMAGE_FILE=${1}
    RPI_IP=${2}
    MOUNT_POINT=/mnt
    DEST_IMG=${MOUNT_POINT}/zImage
    DEVICE=/dev/mmcblk0p1

    echo "[+] Mounting sd-card on the remote Raspberry Pi..."
    ssh -o StrictHostKeyChecking=no root@${RPI_IP} "/bin/mount ${DEVICE} ${MOUNT_POINT}"
    echo "[+] Uploading system image on the remote Raspberry Pi..."
    scp -o StrictHostKeyChecking=no ${IMAGE_FILE} root@${RPI_IP}:${DEST_IMG}
    echo "[+] Unounting sd-card on the remote Raspberry Pi..."
    ssh -o StrictHostKeyChecking=no root@${RPI_IP} "/bin/umount ${MOUNT_POINT}"
    echo "[+] Rebooting the remote Raspberry Pi..."
    ssh -o StrictHostKeyChecking=no root@${RPI_IP} "/sbin/reboot"
}

if ((${#} < 2)); then
    echo "Usage: ${0} file_img rpi_ip"
else
    # Example:
    # ./update_zimage.sh ../buildroot/output/images/zImage 192.168.0.1
    upgrade ${1} ${2}
fi
