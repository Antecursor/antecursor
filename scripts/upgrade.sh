#!/bin/bash

# Upgrade Antecursor.

set -o errexit

if ((${#} < 4)); then
    echo "Usage: ${0} <access_token> <rpi_ip> <branch> <fast|slow> [-f|--force]"
else
    # Example:
    # ./upgrade.sh ${ACCESS_TOKEN} 192.168.0.1 dev slow --force
    ACCESS_TOKEN=${1}
    RPI_IP=${2}
    REF=${3}
    MODE=${4}
    FORCE=${5}
    JOB=antecursor-build
    BASE_PATH=$(dirname $(readlink -f $0))
    DEST_PATH=${BASE_PATH}/builds
    FILE_PATH=${DEST_PATH}/${JOB}-${REF}.zip
    HELPERS_PATH=${BASE_PATH}/helpers

    echo "[+] Looking for updates..."

    if [[ ${FORCE} == "-f" || ${FORCE} == "--force" ]]; then
        set +o errexit
    fi

    ${HELPERS_PATH}/download_last_build.sh ${ACCESS_TOKEN} ${REF} ${DEST_PATH}
    set -o errexit

    echo "[+] Unpacking updates..."
    rm -rf ${DEST_PATH}/${REF}/
    unzip ${FILE_PATH} -d ${DEST_PATH}/${REF}/

    if [[ ${MODE} == "slow" ]]; then
        echo "[+] Update sdcard..."
        ${HELPERS_PATH}/update_sdcard.sh ${DEST_PATH}/${REF}/buildroot/output/images/sdcard.img ${RPI_IP}
    else
        echo "[+] Update zImage..."
        ${HELPERS_PATH}/update_zimage.sh ${DEST_PATH}/${REF}/buildroot/output/images/zImage ${RPI_IP}
    fi

    echo "[+] Antecursor updated successfully you should be able to create SSH connection using ${DEST_PATH}/${REF}/buildroot/.ssh/config"
    echo "[?] Example: ssh root@${RPI_IP} -o StrictHostKeyChecking=no -i ${DEST_PATH}/${REF}/buildroot/.ssh/antecursor_rsa"
fi
