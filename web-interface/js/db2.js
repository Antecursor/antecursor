function handleFileSelect(evt) {
    // get the selected file
    var f = evt.target.files[0];

    // transform selected file into sql.js data (ByteArray)
    var r = new FileReader();
    r.readAsArrayBuffer(f);
    r.onload = function () {
        Uints = new Uint8Array(r.result);
        db = new SQL.Database(Uints);

        displayDB(db);
    };
}

function displayDB(db) {
    // get from DB and display ip addresses
    res = db.exec('SELECT * FROM Credentials ORDER BY src_ip;');

    previous = ''
    out = '';
    res[0].values.forEach(function (row) {
        if (previous != row[0]){
            out += '</div></div><div class="card m-5" id="data-container"><div class="card-header bg-antecursor-g">' + row[0] + ' ----> ' + row[1] +'</div>' + '<div class="list-group list-group-flush">' ;
            previous = row[0] ;
        }
        out += '<li class="list-group-item bg-antecursor-d">' + row[4] + '</li>'
    });
    out += '</div></div>'
    document.getElementById('addresses').innerHTML = out;
}

document.getElementById('file').addEventListener('change', handleFileSelect, false);